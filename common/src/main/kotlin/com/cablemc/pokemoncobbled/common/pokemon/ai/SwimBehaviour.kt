/*
 * Copyright (C) 2022 Pokemon Cobbled Contributors
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.cablemc.pokemoncobbled.common.pokemon.ai

class SwimBehaviour {
    val canSwimInWater = true
    val canSwimInLava = true
    val swimSpeed = 0.3F
    val canBreatheUnderwater = false
    val canBreatheUnderlava = false
    val canWalkOnWater = false
    val canWalkOnLava = false
}