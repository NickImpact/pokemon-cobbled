
plugins {
    base
    id("pokemoncobbled.root-conventions")
}

group = "com.cablemc.pokemoncobbled"
version = "${project.property("mod_version")}+${project.property("mc_version")}"